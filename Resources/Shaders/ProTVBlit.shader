﻿Shader "Hidden/ProTV/Blit"
{
    Properties
    {
        [HideInInspector] _MainTex ("Blit Texture", 2D) = "black" {}
        // hidden in inspector because this should only be assigned via script
        [HideInInspector] _MainTex_ST_Override ("Scale/Offset Override", Vector) = (1.0, 1.0, 0.0, 0.0)
    }
    SubShader
    {
        Pass
        {
            Name "Video Correction"
            // required for Blit to succeed on Android
            ZTest Always
            ZWrite Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _MainTex_ST_Override;

            float _AVPro;
            float _SkipGamma;
            float4 _GammaZone;

            struct vertdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct fragdata
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            fragdata vert(vertdata v)
            {
                fragdata o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            float4 frag(fragdata i) : SV_Target
            {
                // because default properties are stupid while doing a Blit, allow a custom _ST override vector if the provided value is not the default one
                if (any(_MainTex_ST_Override != float4(1, 1, 0, 0))) _MainTex_ST = _MainTex_ST_Override;

                // scale/offset math for AVPro flip correction (this is the same as the TRANSFORM_TEX macro)
                float4 tex = tex2D(_MainTex, i.uv * _MainTex_ST.xy + _MainTex_ST.zw);

                // check that the current UV is within the defined Gamma Zone
                bool uvCheck = all(i.uv >= _GammaZone.zw) && all(i.uv <= _GammaZone.zw + _GammaZone.xy);

                #ifndef UNITY_COLORSPACE_GAMMA
                // if not in gamma colorspace, handle avpro conversion
                tex.rgb = _AVPro && !_SkipGamma && uvCheck ? GammaToLinearSpace(tex.rgb) : tex.rgb;
                #endif

                return tex;
            }
            ENDCG
        }

        Pass
        {
            Name "Video Bake"
            // required for Blit to succeed on Android
            ZTest Always
            ZWrite Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _MainTex_TexelSize;

            float _Brightness;
            float _ForceAspect;
            int _3D;

            struct vertdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct fragdata
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            float2 TVAspectRatio(float2 uv, const float expected_aspect, const float2 res, const float2 center)
            {
                if (expected_aspect == 0) return uv; // apsect of 0 means no adjustments made
                if (abs(res.x / res.y - expected_aspect) <= 0.0001) return uv;
                // if res is close enough, skip adjustment
                float2 norm_res = float2(res.x / expected_aspect, res.y);
                const float2 correction = lerp(
                    // width needs corrected
                    float2(norm_res.x / norm_res.y, 1),
                    // height needs corrected
                    float2(1, norm_res.y / norm_res.x),
                    // determine corrective axis
                    norm_res.x > norm_res.y
                );
                // apply normalized correction anchored to given center
                return ((uv - center) / correction) + center;
            }

            float TVAspectVisibility(const float2 uv, const float2 res, float4 uvClip)
            {
                // make the span of the anti-alias fix span the size of 2 pixel of the source texture
                const float2 uvPadding = (2 / res);
                // get the amount of presence that the uv has on the minimum side, use uvClip to letterbox/pillarbox the visibility
                const float2 minFactor = smoothstep(uvClip.xy, uvClip.xy + uvPadding, uv);
                // get the amount of presence that the uv has on the maximum side, use uvClip to letterbox/pillarbox the visibility
                const float2 maxFactor = smoothstep(uvClip.zw, uvClip.zw - uvPadding, uv);
                // multiply them all together. If any of the factor edges are 0, it is considered not visible
                return maxFactor.x * maxFactor.y * minFactor.x * minFactor.y;
            }

            fragdata vert(const vertdata v)
            {
                fragdata o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            float4 frag(const fragdata i) : SV_Target
            {
                float2 videoDims = _MainTex_TexelSize.zw;
                const int _3d = abs(_3D);
                const int wide = sign(_3D);
                float2 uv = i.uv;
                // default clip zone is 100 % of the uv space
                float4 uvClip = float4(0, 0, 1, 1);

                if (_3d == 1 || _3d == 2)
                {
                    // clip adjustment
                    uvClip.x = 0;
                    uvClip.z = 0.5;
                    // eye correction
                    uv.x = uv.x * 0.5;
                    // correct for SBS-Full mode
                    videoDims.x = lerp(videoDims.x, videoDims.x / 2, wide < 0);
                }
                else if (_3d == 3 || _3d == 4) // over-under
                {
                    // clip adjustment
                    uvClip.y = 0;
                    uvClip.w = 0.5;
                    // eye correction
                    uv.y = uv.y * 0.5;
                    // correct for OVUN-Full mode
                    videoDims.x = lerp(videoDims.y, videoDims.y / 2, wide < 0);
                }

                // determine the effective center of the uv
                const float2 uvCenter = (uvClip.xy + uvClip.zw) * 0.5;

                if (_ForceAspect != 0) uv = TVAspectRatio(uv, _ForceAspect, videoDims, uvCenter);

                const float visible = TVAspectVisibility(uv, videoDims, uvClip);
                // letterbox any uv point that is outside the expected clip zone
                if (!visible) return float4(0, 0, 0, 1);
                float4 tex = tex2D(_MainTex, uv);
                tex = lerp((0).xxxx, tex, visible);
                return tex * _Brightness;
            }
            ENDCG
        }
    }
    Fallback Off
}