﻿Shader "Hidden/ProTV/VideoScreen2"
{
    Properties
    {
        _MainTex("Standby Texture", 2D) = "black" {}
        _SoundTex("Sound-Only Texture", 2D) = "grey" {}
        _VideoTex("Video Texture (Render Texture from the TV goes here)", 2D) = "" {}
        _Aspect("Target Aspect Ratio (0 to ignore)", Float) = 1.77777
        _Brightness("Screen Brightness", Float) = 1
        _GIBrightness("Global Illumination Brightness", Float) = 3
        [Enum(Disabled, 0, Standard, 1, Dynamic, 2)] _Mirror("Mirror Flip Mode", Float) = 1
        [Enum(None, 0, Side by Side, 1, Side By Side Swapped, 2, Over Under, 3, Over Under Swapped, 4)] _3D("Standby 3D Mode", Float) = 0
        [Enum(Half Size 3D, 2, Full Size 3D, 0)] _Wide("Standby 3D Mode Size", Float) = 2
        [ToggleUI] _Force2D("Force Standby to 2D", Float) = 0
        [ToggleUI] _Clip("Clip Aspect", Float) = 0
        [ToggleUI] _Fog("Enable Fog", Float) = 1
        [Enum(UnityEngine.Rendering.CullMode)] _Cull("Culling", Int) = 2
    }
    SubShader
    {
        Tags
        {
            "PerformanceChecks" = "False"
        }
        Pass
        {
            Name "STANDARD"
            Tags
            {
                "Queue" = "AlphaTest+50" "LightMode" = "ForwardBase"
            }
            Cull [_Cull]
            CGPROGRAM
            // GPU Instancing support https://docs.unity3d.com/2022.3/Documentation/Manual/gpu-instancing-shader.html
            #pragma multi_compile_instancing
            #pragma multi_compile_fog
            #pragma vertex vertBase
            #pragma fragment fragBase
            #include "Packages/dev.architech.protv/Resources/Shaders/ProTVCore.cginc"

            // Use explicit sampler state to deal with the texture resizing
            Texture2D _VideoTex;
            SamplerState sampler_VideoTex;
            float4 _VideoTex_ST;
            float4x4 _VideoData;

            float _Clip;
            float _Aspect;
            float _Brightness;

            struct vertdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct fragdata
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
                // fog support
                UNITY_FOG_COORDS(1)
            };

            fragdata vertBase(vertdata v)
            {
                fragdata o;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_TRANSFER_INSTANCE_ID(v, o);
                UNITY_INITIALIZE_OUTPUT(fragdata, o);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                o.vertex = UnityObjectToClipPos(v.vertex);
                // fog support
                UNITY_TRANSFER_FOG(o, o.vertex);
                o.uv = v.uv;
                return o;
            }

            float4 fragBase(const fragdata i) : SV_Target
            {
                // - Run fragment solver
                // - Adjust for fog
                // - Apply brightness adjustment

                UNITY_SETUP_INSTANCE_ID(i);
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);

                fragproc data;
                data.inputUV = i.uv;
                data.videoTexture = _VideoTex;
                data.videoSampler = sampler_VideoTex;
                data.videoST = _VideoTex_ST;
                data.videoData = _VideoData;
                data.outputAspect = _Aspect;
                data.removeBorders = _Clip;
                float4 tex = ProcessFragment(data);
                tex = tex * _Brightness;
                // apply fog adjustment
                #if defined(FOG_LINEAR) || defined(FOG_EXP) || defined(FOG_EXP2)
                    if (_Fog) UNITY_APPLY_FOG(i.fogCoord, tex);
                #endif
                // final color
                return tex;
            }
            ENDCG
        }

        // ------------------------------------------------------------------
        // Extracts information for lightmapping, GI (emission, albedo, ...)
        // This pass is not used during regular rendering.
        Pass
        {
            Name "META"
            Tags
            {
                "LightMode" = "Meta"
            }

            Cull Off

            CGPROGRAM
            #pragma vertex vert_meta
            #pragma fragment frag_meta2

            #pragma shader_feature EDITOR_VISUALIZATION
            #include "UnityStandardMeta.cginc"
            #include "Packages/dev.architech.protv/Resources/Shaders/ProTVCore.cginc"

            // Use explicit sampler state to deal with the texture resizing
            Texture2D _VideoTex;
            SamplerState sampler_VideoTex;
            float4 _VideoTex_ST;
            float4x4 _VideoData;

            float _Clip;
            float _Aspect;
            float _Brightness;
            float _GIBrightness;

            float4 frag_meta2(const v2f_meta i): SV_Target
            {
                UnityMetaInput o = (UnityMetaInput)0;

                fragproc procData;
                procData.inputUV = i.uv;
                procData.videoTexture = _VideoTex;
                procData.videoSampler = sampler_VideoTex;
                procData.videoST = _VideoTex_ST;
                procData.videoData = _VideoData;
                procData.outputAspect = _Aspect;
                procData.removeBorders = _Clip;
                float4 tex = ProcessFragment(procData);

                #ifdef EDITOR_VISUALIZATION
                    o.Albedo = half3(tex.rgb) * _Brightness;
                    o.VizUV = i.vizUV;
                    o.LightCoord = i.lightCoord;
                #else
                o.Albedo = half3(tex.rgb) * _Brightness;
                #endif
                o.Emission = half3(tex.rgb) * _GIBrightness;
                return UnityMetaFragment(o);
            }
            ENDCG
        }

        Pass
        {
            Name "SHADOWCASTER"
            Tags
            {
                "LightMode" = "ShadowCaster"
            }
            CGPROGRAM
            #pragma vertex vertShadow
            #pragma fragment fragShadow
            #pragma multi_compile_instancing
            #pragma multi_compile_shadowcaster
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
            };

            v2f vertShadow(const appdata v)
            {
                v2f o = (v2f)0;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                TRANSFER_SHADOW_CASTER_NORMALOFFSET(o)
                return o;
            }

            float4 fragShadow(v2f i) : SV_Target
            {
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
                return 0;
            }
            ENDCG
        }
    }
    FallBack "VertexLit"
    CustomEditor "VideoScreenGUI"
}