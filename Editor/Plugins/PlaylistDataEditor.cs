using ArchiTech.SDK.Editor;
using UnityEditor;
using UnityEngine;
using VRC.SDKBase;

namespace ArchiTech.ProTV.Editor
{
    [CustomEditor(typeof(PlaylistData))]
    public class PlaylistDataEditor : UnityEditor.Editor
    {
        private PlaylistData script;

        public void OnEnable()
        {
            script = (PlaylistData)target;
            if (script.mainUrls == null) script.mainUrls = new VRCUrl[0];
            if (script.alternateUrls == null) script.alternateUrls = new VRCUrl[0];
            if (script.titles == null) script.titles = new string[0];
            if (script.tags == null) script.tags = new string[0];
            if (script.descriptions == null) script.descriptions = new string[0];
            if (script.images == null) script.images = new Sprite[0];
        }
        public override void OnInspectorGUI()
        {
            EditorGUILayout.LabelField($"Currently storing {script.mainUrls.Length} entries.");
        }
    }
}