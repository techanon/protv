using System;
using System.Collections.Generic;
using System.Linq;
using ArchiTech.SDK.Editor;
using UnityEditor;
using UnityEngine;

namespace ArchiTech.ProTV.Editor
{
    [CustomEditor(typeof(TVPlugin), true)]
    public class TVPluginEditor : ATEventHandlerEditor
    {
        private TVPlugin script;

        protected void SetupTVReferences()
        {
            script = (TVPlugin)target;
        }

        [Obsolete("Use ATEditorUtility.GetComponentsInSceneWithDistinctNames<T>(out string[]) instead.")]
        public static T[] GetAllDistinctComponentsInScene<T>(out string[] names) where T : Component =>
            ATEditorUtility.GetComponentsInSceneWithDistinctNames<T>(out names);

        /// <summary>
        /// Draws the TV property will auto-detection dropdown, optionally draws the Queue property if it also exists on the component.
        /// </summary>
        /// <param name="includePlaylist">flag whether playlist should also be drawn if it exists, defaults to true</param>
        /// <param name="includeQueue">flag whether queue should also be drawn if it exists, defaults to true</param>
        /// <returns>whether or not any of the variables have been modified</returns>
        protected bool DrawTVReferences(bool includePlaylist = true, bool includeQueue = true)
        {
            if (script == null) SetupTVReferences();
            bool isChanged = false;
            using (SectionScope("TV References"))
            {
                if (serializedObject.TryFindProperty("tv", out _))
                    isChanged |= DrawTVDropdown();
                if (includePlaylist && serializedObject.TryFindProperty("playlist", out _))
                    isChanged |= DrawPlaylistDropdown();
                if (includeQueue && serializedObject.TryFindProperty("queue", out _))
                    isChanged |= DrawQueueDropdown();
            }
            return isChanged;
        }

        protected bool DrawTVDropdown() => DrawVariableWithDropdown("tv");
        protected bool DrawPlaylistDropdown() => DrawVariableWithDropdown("playlist");
        protected bool DrawQueueDropdown() => DrawVariableWithDropdown("queue");

        private void OnEnable()
        {
            SetupTVReferences();
        }

        protected override void RenderChangeCheck()
        {
            DrawTVReferences();
        }

        [Obsolete]
        protected void DrawCoreReferences() => DrawTVReferences();

        [Obsolete]
        protected void SetupCoreReferences() => SetupTVReferences();
    }
}