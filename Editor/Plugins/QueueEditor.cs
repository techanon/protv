using UnityEditor;

namespace ArchiTech.ProTV.Editor
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(Queue))]
    public class QueueEditor : TVPluginEditor
    {
        protected override bool autoRenderVariables => false;

        protected override void RenderChangeCheck()
        {
            var script = (Queue)target;

            DrawTVReferences();

            using (SectionScope("General Settings"))
            {
                DrawVariablesByName(
                    nameof(script.maxQueueLength),
                    nameof(script.preventDuplicateVideos),
                    nameof(script.enableAddWhileLocked),
                    nameof(script.openEntrySelection),
                    nameof(script.showUrlsInQueue),
                    nameof(script.loop));
            }

            using (SectionScope("Per-Player Settings"))
            {
                DrawVariablesByName(
                    nameof(script.maxEntriesPerPlayer),
                    nameof(script.maxBurstEntriesPerPlayer));

                if (script.maxBurstEntriesPerPlayer > 0)
                    DrawVariablesByName(nameof(script.burstThrottleTime));
            }
        }
    }
}