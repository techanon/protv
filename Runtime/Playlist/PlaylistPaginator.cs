﻿using ArchiTech.SDK;
using UdonSharp;
using UnityEngine;

namespace ArchiTech.ProTV
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.NoVariableSync)]
    public class PlaylistPaginator : ATBehaviour
    {
        public Playlist playlist;
        private RectTransform listContainer;
        private int perRow;
        private int perColumn;


        public override void Start()
        {
            if (init) return;
            base.Start();

            if (playlist == null)
            {
                SetLogPrefixLabel("<Missing Playlist Ref>");
                Warn("Must specify playlist");
                return;
            }

            listContainer = playlist.listContainer;
            Rect max = playlist.scrollView.viewport.rect;
            Rect item = ((RectTransform)playlist.template.transform).rect;
            perRow = Mathf.FloorToInt(max.width / item.width);
            if (perRow == 0) perRow = 1;
            perColumn = listContainer.childCount / perRow;
            SetLogPrefixLabel(playlist.gameObject.name);
        }

        public void _PrevPage()
        {
            if (!init) return;
            seekView(perRow * perColumn * -1 + 1);
        }

        public void _PrevRow()
        {
            if (!init) return;
            seekView(-perRow);
        }

        public void _NextRow()
        {
            if (!init) return;
            seekView(perRow);
        }

        public void _NextPage()
        {
            if (!init) return;
            seekView(perRow * perColumn - 1);
        }

        private void seekView(int shift)
        {
            playlist.SeekView(playlist.viewOffset + shift);
        }
    }
}