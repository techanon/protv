﻿using System;
using UnityEngine;

namespace ArchiTech.ProTV
{
    [AddComponentMenu(""), Obsolete("Use TVManagedWhitelist instead")]
    public class TVUsernameWhitelist : TVManagedWhitelist { }
}