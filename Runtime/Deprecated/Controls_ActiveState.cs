﻿using System;
using ArchiTech.ProTV;
using UnityEngine;

namespace ArchiTech
{
    [AddComponentMenu(""), Obsolete("Deprecated component type. Will be updated to MediaControls component.")]
    public class Controls_ActiveState : MediaControls { }
}