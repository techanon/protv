using System;
using UnityEngine;

namespace ArchiTech.ProTV
{
    [AddComponentMenu(""), Obsolete("Deprecated component type. Will be updated to AudioAdapter. You can manually update it yourself in the component context menu.")]
    public class AudioLinkAdapter : AudioAdapter { }
}