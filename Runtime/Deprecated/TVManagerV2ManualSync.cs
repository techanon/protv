using System;
using ArchiTech.ProTV;
using UnityEngine;

namespace ArchiTech
{
    [AddComponentMenu(""), Obsolete("Deprecated component type. Will be updated to TVManagerData component.")]
    public class TVManagerV2ManualSync : TVManagerData { }
}