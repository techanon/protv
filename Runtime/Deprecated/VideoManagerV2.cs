﻿using System;
using ArchiTech.ProTV;
using UnityEngine;

namespace ArchiTech
{
    [AddComponentMenu(""), Obsolete("Deprecated component type, switch to VPManager instead")]
    public class VideoManagerV2 : VPManager { }
}