﻿using System;
using ArchiTech.SDK;
using JetBrains.Annotations;
using TMPro;
using UdonSharp;
using UnityEngine;
using UnityEngine.UI;
using VRC.SDKBase;

namespace ArchiTech.ProTV
{
    [UdonBehaviourSyncMode(BehaviourSyncMode.None)]
    public partial class History : TVPlugin
    {
        [NonSerialized] private int IN_INDEX = -1;

        [SerializeField] internal Queue queue;

        [Range(5, 50), SerializeField,
         I18nTooltip("The maximum number of entries the list will retain. All older entries will be removed.")
        ]
        internal int numberOfEntries = 15;

        [SerializeField,
         I18nTooltip("This option allows a URL to be copied IF the corresponding reference is available on the Template object.")
        ]
        internal bool enableUrlCopy;

        [SerializeField,
         I18nTooltip("This option ensures that a URL can only be copied by an authorized user.")
        ]
        internal bool protectUrlCopy;

        [SerializeField,
         I18nTooltip("The label that will be shown in place of an empty title.")
        ]
        internal string emptyTitlePlaceholder = "No Title";

        private VRCUrl[] mainUrls;
        private VRCUrl[] alternateUrls;
        private string[] titles;
        private string[] addedBy;
        private int nextIndex = 0;
        private bool newEntryExpected = false;

        [SerializeField,
         I18nTooltip("Container reference which the history entries will be added to/removed from. It is recommended to have either a VerticalLayoutGroup, HorizontalLayoutGroup or GridLayoutGroup component on this element for easy layout controls.")
        ]
        internal RectTransform listContainer;

        [SerializeField,
         I18nTooltip("Object that will be instantiated for each entry in the history list. Each entry will be parented to the List Container transform upon instantiation.")
        ]
        internal GameObject template;

        [SerializeField,
         I18nTooltip("Text display component for the relevant entry main URL. MUST be a child of the Template object if provided. Supports both UI and TMP.")
        ]
        internal Text urlDisplay;

        [SerializeField,
         I18nTooltip("Text display component for the relevant entry Title. MUST be a child of the Template object. Supports both UI and TMP.")
        ]
        internal Text titleDisplay;

        [SerializeField,
         I18nTooltip("Text display component for the name of the user who added the entry. MUST be a child of the Template object. Supports both UI and TMP.")
        ]
        internal Text addedByDisplay;

        [SerializeField] internal TextMeshProUGUI urlDisplayTMP;
        [SerializeField] internal TextMeshProUGUI titleDisplayTMP;
        [SerializeField] internal TextMeshProUGUI addedByDisplayTMP;

        [SerializeField,
         I18nTooltip("Interaction component for triggering the respective entry. MUST be a child of the Template object.")
        ]
        internal Button restoreAction;

        [SerializeField,
         I18nTooltip("Interaction component for triggering an input field copy. MUST be a child of the Template object.")
        ]
        internal InputField copyAction;

        [SerializeField, HideInInspector] internal string urlDisplayTmplPath;
        [SerializeField, HideInInspector] internal string titleDisplayTmplPath;
        [SerializeField, HideInInspector] internal string addedByDisplayTmplPath;
        [SerializeField, HideInInspector] internal string urlDisplayTMPTmplPath;
        [SerializeField, HideInInspector] internal string titleDisplayTMPTmplPath;
        [SerializeField, HideInInspector] internal string addedByDisplayTMPTmplPath;
        [SerializeField, HideInInspector] internal string restoreActionTmplPath;
        [SerializeField, HideInInspector] internal string copyActionTmplPath;

        private Transform[] entryRefs;
        private Text[] urlDisplayRefs;
        private Text[] titleDisplayRefs;
        private Text[] addedByDisplayRefs;
        private TextMeshProUGUI[] urlDisplayTMPRefs;
        private TextMeshProUGUI[] titleDisplayTMPRefs;
        private TextMeshProUGUI[] addedByDisplayTMPRefs;
        private Button[] restoreActionRefs;
        private InputField[] copyActionRefs;

        private bool hasQueue;
        private bool hasUrlDisplay;
        private bool hasUrlDisplayTMP;
        private bool hasTitleDisplay;
        private bool hasTitleDisplayTMP;
        private bool hasAddedByDisplay;
        private bool hasAddedByDisplayTMP;
        private bool hasRestoreAction;
        private bool hasCopyAction;

        [SerializeField, HideInInspector] internal int _EDITOR_templateUpgrade;

        // wait until all other plugins have finished before considering the history
        public override sbyte Priority => 127;

        private int CurrentIndex => wrap(nextIndex - 1);

        public override void Start()
        {
            if (init) return;
            base.Start();

            mainUrls = new VRCUrl[numberOfEntries];
            alternateUrls = new VRCUrl[numberOfEntries];
            titles = new string[numberOfEntries];
            addedBy = new string[numberOfEntries];

            hasQueue = queue != null;
            hasRestoreAction = restoreAction != null;
            hasCopyAction = copyAction != null;
            hasUrlDisplay = urlDisplay != null;
            hasTitleDisplay = titleDisplay != null;
            hasAddedByDisplay = addedByDisplay != null;
            hasUrlDisplayTMP = urlDisplayTMP != null;
            hasTitleDisplayTMP = titleDisplayTMP != null;
            hasAddedByDisplayTMP = addedByDisplayTMP != null;

            entryRefs = new Transform[numberOfEntries];
            if (hasRestoreAction) restoreActionRefs = new Button[numberOfEntries];
            if (hasCopyAction) copyActionRefs = new InputField[numberOfEntries];
            if (hasUrlDisplay) urlDisplayRefs = new Text[numberOfEntries];
            if (hasTitleDisplay) titleDisplayRefs = new Text[numberOfEntries];
            if (hasAddedByDisplay) addedByDisplayRefs = new Text[numberOfEntries];
            if (hasUrlDisplayTMP) urlDisplayTMPRefs = new TextMeshProUGUI[numberOfEntries];
            if (hasTitleDisplayTMP) titleDisplayTMPRefs = new TextMeshProUGUI[numberOfEntries];
            if (hasAddedByDisplayTMP) addedByDisplayTMPRefs = new TextMeshProUGUI[numberOfEntries];

            if (template != null) template.SetActive(false);
        }

        #region TV Events

        public override void _TvMediaReady()
        {
            // short circut for generic refreshes or video player swaps
            var main = tv.urlMain;
            var localMain = mainUrls[CurrentIndex];
            if (!newEntryExpected || localMain != null && localMain.Get() == main.Get()) return;
            mainUrls[nextIndex] = tv.urlMain;
            alternateUrls[nextIndex] = tv.urlAlt;
            titles[nextIndex] = tv.title;
            addedBy[nextIndex] = tv.addedBy;
            nextIndex = wrap(nextIndex + 1);
            updateUI();
        }

        public override void _TvTitleChange()
        {
            var currentIndex = CurrentIndex;
            // update the title of the current entry if available and no new entry is expected
            if (!newEntryExpected && mainUrls[currentIndex] != null)
            {
                titles[currentIndex] = OUT_TITLE;
            }
        }

        public override void _TvMediaChange()
        {
            newEntryExpected = true;
        }

        public override void _TvAuthChange() => updateUI();

        public override void _TvLock() => updateUI();

        public override void _TvUnLock() => updateUI();

        #endregion

        private void updateUI()
        {
            int index = wrap(nextIndex - 1);
            var copyAllowed = enableUrlCopy && (!protectUrlCopy || tv.CanPlayMedia);
            for (int i = 0; i < numberOfEntries; i++)
            {
                var mainUrl = mainUrls[index];
                if (mainUrl == null) continue;
                var mainUrlStr = mainUrl.Get();
                var altUrlStr = alternateUrls[index].Get();
                if (i >= listContainer.childCount) addEntry();
                if (hasUrlDisplay) urlDisplayRefs[i].text = mainUrls[index].Get();
                if (hasTitleDisplay)
                {
                    var title = titles[index];
                    titleDisplayRefs[i].text = string.IsNullOrEmpty(title) ? emptyTitlePlaceholder : title;
                }

                if (hasAddedByDisplay) addedByDisplayRefs[i].text = addedBy[index];
                if (hasUrlDisplayTMP) urlDisplayTMPRefs[i].text = mainUrls[index].Get();
                if (hasTitleDisplayTMP)
                {
                    var title = titles[index];
                    titleDisplayTMPRefs[i].text = string.IsNullOrEmpty(title) ? emptyTitlePlaceholder : title;
                }

                if (hasAddedByDisplayTMP) addedByDisplayTMPRefs[i].text = addedBy[index];
                // if both URLs match the TV, disable the restore action since that would be ignored internally to the TV
                // doubles as a signal to the user that the entry is currently what is playing on the TV
                if (hasRestoreAction) restoreActionRefs[i].gameObject.SetActive(tv.CanPlayMedia && (mainUrlStr != tv.urlMain.Get() || altUrlStr != tv.urlAlt.Get()));
                if (hasCopyAction)
                {
                    var copyRef = copyActionRefs[i];
                    copyRef.gameObject.SetActive(copyAllowed);
                    copyRef.enabled = copyAllowed;
                    copyRef.text = copyAllowed ? mainUrlStr : EMPTYSTR;
                }

                index = wrap(index - 1);
            }
        }

        private int wrap(int value)
        {
            value %= numberOfEntries;
            if (value < 0) value += numberOfEntries;
            return value;
        }

        private void addEntry()
        {
            var index = listContainer.childCount;
            var go = Instantiate(template);
            go.name = $"Entry ({index})";
            go.SetActive(true);
            var entry = go.transform;
            entry.SetParent(listContainer, false);

            entryRefs[index] = entry;
            Transform t;
            if (hasRestoreAction)
            {
                t = entry;
                if (restoreActionTmplPath != EMPTYSTR) t = entry.Find(restoreActionTmplPath);
                restoreActionRefs[index] = t.GetComponent<Button>();
            }

            if (hasCopyAction)
            {
                t = entry;
                if (copyActionTmplPath != EMPTYSTR) t = entry.Find(copyActionTmplPath);
                copyActionRefs[index] = t.GetComponent<InputField>();
            }

            if (hasUrlDisplay)
            {
                t = entry;
                if (urlDisplayTmplPath != EMPTYSTR) t = entry.Find(urlDisplayTmplPath);
                urlDisplayRefs[index] = t.GetComponent<Text>();
            }

            if (hasTitleDisplay)
            {
                t = entry;
                if (titleDisplayTmplPath != EMPTYSTR) t = entry.Find(titleDisplayTmplPath);
                titleDisplayRefs[index] = t.GetComponent<Text>();
            }

            if (hasAddedByDisplay)
            {
                t = entry;
                if (addedByDisplayTmplPath != EMPTYSTR) t = entry.Find(addedByDisplayTmplPath);
                addedByDisplayRefs[index] = t.GetComponent<Text>();
            }

            if (hasUrlDisplayTMP)
            {
                t = entry;
                if (urlDisplayTMPTmplPath != EMPTYSTR) t = entry.Find(urlDisplayTMPTmplPath);
                urlDisplayTMPRefs[index] = t.GetComponent<TextMeshProUGUI>();
            }

            if (hasTitleDisplayTMP)
            {
                t = entry;
                if (titleDisplayTMPTmplPath != EMPTYSTR) t = entry.Find(titleDisplayTMPTmplPath);
                titleDisplayTMPRefs[index] = t.GetComponent<TextMeshProUGUI>();
            }

            if (hasAddedByDisplayTMP)
            {
                t = entry;
                if (addedByDisplayTMPTmplPath != EMPTYSTR) t = entry.Find(addedByDisplayTMPTmplPath);
                addedByDisplayTMPRefs[index] = t.GetComponent<TextMeshProUGUI>();
            }

            if (hasRestoreAction)
            {
                t = entry;
                if (restoreActionTmplPath != EMPTYSTR) t = entry.Find(restoreActionTmplPath);
                restoreActionRefs[index] = t.GetComponent<Button>();
            }
        }

        [PublicAPI]
        public void Clear()
        {
            // purge all except the latest media which would still be referenced interally to the TV
            // since the whole list is being purged, reset the ring buffer index
            var currentIndex = CurrentIndex;
            var mainUrl = mainUrls[currentIndex];
            var altUrl = alternateUrls[currentIndex];
            var title = titles[currentIndex];
            System.Array.Clear(mainUrls, 0, mainUrls.Length);
            System.Array.Clear(alternateUrls, 0, alternateUrls.Length);
            System.Array.Clear(titles, 0, titles.Length);
            mainUrls[0] = mainUrl;
            alternateUrls[0] = altUrl;
            titles[0] = title;
            nextIndex = 1;
            int count = listContainer.childCount;
            while (count > 1) Destroy(listContainer.GetChild(--count).gameObject);
            updateUI();
        }

        public void SelectEntry()
        {
            if (IN_INDEX == -1) IN_INDEX = getDetectedEntry(restoreActionRefs);
            SelectEntry(IN_INDEX);
            IN_INDEX = -1;
        }

        [PublicAPI]
        public void SelectEntry(int index)
        {
            if (index == -1) return; // bad index value
            if (index >= listContainer.childCount) return; // bad index value
            index = wrap(CurrentIndex - index); // convert the entry index into the ring-buffer index
            if (hasQueue) queue._AddEntry(mainUrls[index], alternateUrls[index], titles[index]);
            else tv._ChangeMedia(mainUrls[index], alternateUrls[index], titles[index]);
        }

        private int getDetectedEntry(Selectable[] referencesArray)
        {
            if (IsTraceEnabled) Trace("Auto-detecting selected index via interaction");
            for (int i = 0; i < referencesArray.Length; i++)
            {
                var @ref = referencesArray[i];
                if (@ref == null) continue;
                if (!@ref.enabled)
                {
                    var index = entryRefs[i].GetSiblingIndex();
                    if (IsTraceEnabled) Trace($"Detected index {index}");
                    return index;
                }
            }

            if (IsTraceEnabled) Trace("Index not able to be auto-detected");
            return -1;
        }
    }
}